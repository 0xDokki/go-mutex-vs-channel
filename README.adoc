= Mutex vs Channel

In this small project I want to find out what the answer to the following question is:

> What is the fastest way that many goroutines can store result data
> in a common output structure (i.e. a slice)

As the title implies I'll mainly be looking at these two techniques:

. Mutex
** The destination is mutexed.
 Each source goroutine acquires the mutex when done processing, appends its data to the destination and then releases the mutex.
. Channel
** Each source goroutine submits its data to a common (buffered) channel.
 A separate goroutine collects this data and concatenates it to the destination.

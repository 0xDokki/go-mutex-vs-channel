package main

import "math/rand"

var (
	dp       DataProvider
	cp1, cp2 Copier
)

func main() {
	dp = &RandomDataProvider{}
	cp1 = &MutexCopier{}
	cp2 = &ChannelCopier{}

	_ = dp
	_ = cp1
	_ = cp2

	dp.Setup(8, 4)
	cp2.Setup(dp, 4, 1, 4)

	l := make([]string, 0)

	cp2.Copy(&l)

}

type RandomDataProvider struct {
	buffer [][]string
}

func (rdp *RandomDataProvider) Setup(n, l int) {
	bufferSize := 32

	rdp.buffer = make([][]string, bufferSize)
	for i := 0; i < bufferSize; i++ {
		rdp.buffer[i] = make([]string, n)
		for j := 0; j < n; j++ {
			rdp.buffer[i][j] = StringOfLength(l)
		}
	}
}

func (rdp *RandomDataProvider) ProvideString() []string {
	n := rand.Intn(len(rdp.buffer))
	return rdp.buffer[n]
}
